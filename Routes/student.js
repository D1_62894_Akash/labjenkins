const express = require('express')
const router = express.Router()
const db = require('../db')
const utils = require('../utils')
router.post('/', (request, response) => {
    const { roll_no, name, Class, division, dateofbirth, parent_mobile_no } =
        request.body
    const statement = `INSERT INTO student(roll_no, name, Class, division, dateofbirth, parent_mobile_no)
                       VALUES(?,?,?,?,?,?)`

    db.pool.query(
        statement,
        [roll_no, name, Class, division, dateofbirth, parent_mobile_no],
        (error, result) => {
            response.send(utils.createReult(error, result))
        }
    )
})
router.get('/:roll_no/:division/:Class', (request, response) => {
    const { roll_no, division, Class } = request.params
    const statement = `SELECT name,division,dateofbirth,parent_mobile_no
                        FROM student WHERE roll_no=? AND (division=? AND Class=?)`
    db.pool.query(statement, [roll_no, division, Class], (error, student) => {
        response.send(utils.createReult(error, student))
    })
})
router.put('/:roll_no/:Class/:division', (request, response) => {
    const { roll_no, Class, division } = request.params
    const setClass = request.body.Class
    const setRoll_no = request.body.roll_no
    const setDivision = request.body.division

    const statement = `UPDATE student SET
                       Class=?,division=?,roll_no=? WHERE roll_no=? AND (Class=? AND division=?)`

    db.pool.query(
        statement,
        [setClass, setDivision, setRoll_no, roll_no, Class, division],
        (error, student) => {
            response.send(utils.createReult(error, student))
        }
    )
})
router.get('/:Class', (request, response) => {
    const { Class } = request.params
    const statement = `SELECT roll_no,name,division,dateofbirth,parent_mobile_no 
                         FROM student WHERE Class=?`

    db.pool.query(statement, [Class], (error, result) => {
        response.send(utils.createReult(error, result))
    })
})
router.get('/', (request, response) => {
    const { year } = request.query

    const statement = `SELECT roll_no,name,Class,division,dateofbirth,parent_mobile_no FROM student
                         WHERE year(dateofbirth)=?`

    db.pool.query(statement, [year], (error, students) => {
        response.send(utils.createReult(error, students))
    })
})
router.delete('/', (request, response) => {
    const { roll_no, division, Class } = request.body

    const statement = `DELETE FROM Student WHERE roll_no=? AND (division=? AND Class=?)`

    db.pool.query(statement, [roll_no, division, Class], (error, result) => {
        response.send(utils.createReult(error, result))
    })
})

module.exports = router