function createReult(error, data) {
    const result = {}
    if (error) {
      if (error.code == 'ER_DUP_ENTRY') {
        result['error'] = 'error'
        result['msg'] = 'Roll no is alerdy exist'
      } else {
        result['error'] = 'error'
        result['data'] = error
      }
    } else if (data.length == 0) {
      result['status'] = 'success'
      result['msg'] = 'Student not exist'
    } else {
      result['success'] = 'success'
      result['data'] = data
    }
    return result
  }
  
  module.exports = {
    createReult,
  }
  